#!/usr/bin/python3
import argparse, socket, os, sys, pycurl, io
from threading import Thread,Lock,Event

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

# The scheme holds the details of the test
# which are loaded up from a file
class Scheme:
    def __init__(self, path):

        self.url = ''
        self.data = ''
        self.encoding = 'UTF-8'
        self.fail = []

        if path[0] is '/':
            with open(path) as fp:
                self.parse(fp.read())
        else:
            with open(os.getcwd() + "/" + path) as fp:
                self.parse(fp.read())

    def parse(self, text):
        for l in text.split('\n'):

            if not l or l[0] == '#':
                continue
                
            parts = l.split(':',1)

            tok = parts[0].strip()
            if tok == 'url':
                self.url = parts[1].strip()
            elif tok == 'data':
                self.parse_data(parts[1].strip())
            elif tok == 'fail':
                self.parse_fail(parts[1].strip())

    def parse_data(self, text):
        parts = text.split(':')
        if parts[0] == 'post':
            self.data = {
                'type' : 'post',
                'template' : parts[1],
            }

    def parse_fail(self, text):
        ftype = ''
        fvalue = ''
        for p in text.split(';'):
            parts = p.split(':')
            if parts[0] == 'header':
                header = parts[1].split('=', 1)
                
                self.fail.append({
                    'type': 'header',
                    'header': header[0],
                    'value': header[1],
                })
            elif parts[0] == 'text':
                self.fail.append({
                    'type': 'text',
                    'value': parts[1],
                })

# Testing class that takes the top of a list chain
# an requests each new configuration of data, sending
# it to the target and testing the result against the
# scheme. Each Fmrk is a thread in itself and allows
# for parallel requests
class Fmrk(Thread):
    def __init__(self, scheme, handler, logger):
        super(Fmrk, self).__init__()
        self.scheme = scheme
        self.handler = handler
        self.logger = logger

    def run(self):
        htmp = io.BytesIO()
        btmp = io.BytesIO()
        c = pycurl.Curl()
        c.setopt(c.HEADERFUNCTION, htmp.write)
        c.setopt(c.WRITEFUNCTION, btmp.write)
        c.setopt(pycurl.SSL_VERIFYPEER, 0)
        c.setopt(pycurl.SSL_VERIFYHOST, 0)
        template = self.scheme.data['template']
            
        while True:
            # reset buffers
            htmp.truncate(0)
            btmp.truncate(0)
            
            url = self.scheme.url

            # trigger value cascade
            values = self.handler.next()
            # we've reached the end of all lists
            if values == None:
                break

            concrete = template.replace("^U^", values['user'])
            concrete = concrete.replace("^P^", values['pass'])
            
            if self.scheme.data['type'] == 'post':
                c.setopt(c.POST, 1)
                c.setopt(c.POSTFIELDS, concrete)
            elif self.scheme.data['type'] == 'get':
                url += "?"+concrete
            c.setopt(c.URL, url)


            try:
                c.perform()
                headers = list(map(lambda h: [s.strip().lower() for s in h.split(":",1)],  htmp.getvalue().decode('UTF-8').split("\r\n")))
                body = btmp.getvalue().decode(self.scheme.encoding)
                code = c.getinfo(pycurl.HTTP_CODE);

                if self.has_failed(code, headers, body) == True:
                    out = "{} : {} ... {}nope{}".format(values['user'],values['pass'], bcolors.FAIL, bcolors.ENDC)
                    found = self.logger.out(out, fail=True, creds=None, perc=values['_pos'])
                    if found == True:
                        break
                else:
                    out = "{} : {} ... {}oh yeeeaa!{}".format(values['user'],values['pass'], bcolors.OKGREEN, bcolors.ENDC)
                    self.logger.out(out, fail=False,creds=values, perc=values['_pos'])
                    break
                    
            except pycurl.error:
                break

    def has_failed(self, code, headers, body):
        # loop through each failure state
        for desc in self.scheme.fail:
            if desc['type'] == 'header':
                if self.fail_headers(desc, headers) == True:
                    return True
            elif desc['type'] == 'text':
                if self.fail_text(desc, body) == True:
                    return True
        return False
        
    def fail_headers(self, desc, headers):
        key = desc['header']
        value = desc['value']
        for h in headers:
            if h[0] == key and h[1] == value:
                return True

        return False

    def fail_text(self, desc, body):
        value = desc['value']
        if body.find(value) != -1:
            return True

        return False

# Represents a list of resources (lines) that are grouped
# under a category ('key' e.g. usernames or passwords). lists
# are then linked together into a dependency chain. Values are
# passed up the chain, each list adding it's resource which
# corresponds to its key.
#
# If the list is at the end of the chain and has no dependency
# then every time it is called it will just load the next resource
# in the list until none are left.
#
# If a list has a dependency it will use a single value until it's
# dependency returns None, at which point it will load the next value.
# If, at that point, the list has reached the end it will pass up None.
# This cascades through the chain until each list is exhausted and None
# is finally passed back to the object using the chain.
#
# The list machinary is thread safe as calling next() on the uppermost
# list will lock the entire chain until the next configuration is achieved
class ListHandler():
# ToDo:
# circular buffer N lines
    def __init__(self, lines, key):
        self.lines = lines
        self.key = key
        self.mtx = Lock()
        self.dependency = None
        self.load_next()
        self.initial = True
        self.gpos = 0
        self.glen = 0

    def next(self):
        with self.mtx:
            # At the end of the chain
            if self.dependency == None:

                # If we're in the the initial request
                # at the end of the chain, we need to
                # not load the next one since the line
                # is already loaded. If this check was
                # not here then it would skip the first
                # line
                if self.initial == True:
                    self.initial = False
                else:
                    self.load_next()

                if self.current == None:
                    return None

                self.gpos += 1
                return {
                    # include meta data
                    '_pos' : int((self.gpos / self.glen)*100.0),
                    self.key: self.current,
                }

            block = self.dependency.next()
            # If the dependency has hit the end
            if block == None:
                self.load_next() # load our next one
                if self.current == None:
                    return None # we've reached the end of our list

                # We have another item, so reset the dependency
                # back to the beginning, and this should cascade
                # down the line
                self.dependency.reset() # reset dependency
                block = self.dependency.next() # get the reset value

            block.update({self.key : self.current}) # add current
            return block

    def load_next(self):
        self.current = self.lines.next()

    def reset(self):
        self.lines.reset()
        if self.dependency != None:
            self.dependency.reset()

    def link(self, handler):
        self.dependency = handler

    def len(self):
        return self.lines.len

    def global_len(self, size = 1):
        size *= self.len()
        if self.dependency != None:
            self.dependency.global_len(size)
        else:
            self.glen = size
            print(self.glen)

# Line handler is the interface for reading a line
# at a time from a resource
class LineHandler():
    def next(self):
        raise NotImplementedError("Please Implement this method")
    def pos(self):
        raise NotImplementedError("Please Implement this method")
    def len(self):
        raise NotImplementedError("Please Implement this method")
    def reset(self):
        raise NotImplementedError("Please Implement this method")

# Takes a single line and acts like
# a file with one line
class SingleLine(LineHandler):
    def __init__(self, line):
        self.line = line
        self.pos = 0
        self.len = 1

    def next(self):
        if self.pos == self.len:
            return None
        self.pos += 1
        return self.line

    def pos(self):
        return self.pos

    def len(self):
        return self.len

    def reset(self):
        self.pos = 0

# Takes a stream from a file pointer and
# returns a new line on each passfile:///usr/share/kali-defaults/web/homepage.html
class StreamedLine(LineHandler):
    def __init__(self, path):
        self.fp = open(path, encoding='ascii', errors='ignore')
        self.len = self.calc_len()
        self.reset()

    def next(self):
        if self.pos == self.len:
            return None

        current = self.fp.readline().strip()
        self.pos += 1
        return current

    def pos(self):
        return self.pos

    def len(self):
        return self.len

    def reset(self):
        self.fp.seek(0,0)
        self.pos = 0

    def calc_len(self):
        l = 0
        while True:
            buf = self.fp.read(2048)
            if not buf:
                break
            l += buf.count('\n')

        return l
            
        
        
# The logger class for logging the information
# to stdout and a file
class Logger:
    def __init__(self, verbose):
        self.success = []
        self.found = False
        self.mtx = Lock()
        self.verbose = verbose
        self.fp = open('credentials.lst', 'w')
        self.progress = 0

    def out(self, text, fail, creds, perc):
        with self.mtx:
            if self.verbose == False and perc > self.progress and perc%2 == 0:
                sys.stdout.write("\rProgress: {}%\t\t".format(perc))
                sys.stdout.flush()
                self.progress = perc

            if self.found == False:
                if fail == True and self.verbose == False:
                    return self.found
                if self.verbose == False: print("")
                print("[{}%]\t".format(perc) + text)

            if fail == False:
                self.success.append(creds)
                self.fp.write("{} : {}\n".format(creds['user'], creds['pass']))
                self.found = True

            return self.found

                
            
parser = argparse.ArgumentParser(description='Simple dedicated web form dictionary attacker')

parser.add_argument('scheme', metavar='SCHEME',
                    help='The scheme to run against')

parser.add_argument('-l', '--login', dest='login', metavar="LOGIN", default='', action="store",
                    help='Use specific login')

parser.add_argument('-L', '--lfile', dest='lfile', metavar="LFILE", default='', action="store",
                    help='Use list of logins (login per line)')

parser.add_argument('-P', '--pfile', dest='pfile', metavar="FILE", default='', action="store",
                    help='Use list of passwords (password per line)')

parser.add_argument('-p', '--password', dest='password', metavar="PASS", default='', action="store",
                    help='Use specific password')

parser.add_argument('-t', '--threads', dest='threads', metavar="N", default=4, type=int, action="store",
                    help='Number of threads')

parser.add_argument('-v', '--verbose', dest='verbose', default=False, action="store_true",
                    help='Verbose output')

args = parser.parse_args()
       

if args.login != '':
    uhandler = ListHandler(SingleLine(args.login), 'user')
elif args.lfile != '':
    uhandler = ListHandler(StreamedLine(args.lfile), 'user')

if args.password != '':
    phandler = ListHandler(SingleLine(args.password), 'pass')
elif args.pfile != '':
    phandler = ListHandler(StreamedLine(args.pfile), 'pass')


uhandler.link(phandler)
uhandler.global_len()
print("users / passwords: {} / {}\n".format(uhandler.len(), phandler.len()))

logger = Logger(args.verbose)
scheme = Scheme(args.scheme)

threads = []
for i in range(0, args.threads):
    threads.append(Fmrk(scheme, uhandler, logger))

for fm in threads:
    fm.start()

for fm in threads:
    fm.join


