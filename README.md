
A bit like hydra and medusa but not modular (or any where near as sophisticated) -- it is dedicated to just web forms. I wanted to move the configuration for the form into a file for quicker reuse and then the dictionaries specified on the command line. The configuration is more descriptive in terms of HTTP response and also wanted to have multiple failure states.

Configuration example:
```
# URL to send to
url:  http://localhost/login.php

# parameter template
data: post:user=^U^&pass=^P^

# Fail on header
#fail: header:location=login.php?err=1

# Fail on text
fail: text:Incorrect
```
